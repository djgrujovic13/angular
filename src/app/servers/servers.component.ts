import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss']
})
export class ServersComponent implements OnInit {
  // Define user name details
  userName: string = '';

  // Define server details
  serverName: string = 'Test server';
  serverCreated: boolean = false;
  serverCreationStatus: string = 'No server was created';

  // Adding list of items
  servers = ['Test server 1', 'Test server 2'];

  onServerCreated(): void {
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server is now created ' + this.serverName;
  }



  constructor() {}

  ngOnInit(): void {
  }

}
